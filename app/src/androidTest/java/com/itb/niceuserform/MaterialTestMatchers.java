package com.itb.niceuserform;

import android.view.View;

import com.google.android.material.textfield.TextInputLayout;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;


/**
 * Matchers for Material Views
 */
public class MaterialTestMatchers {
    /**
     * Test that an error is present on an inputLayout
     * onView(withId(R.id.textInputLayoutId)).check(matches(hasTextInputLayoutErrorText(R.string.some_error)));
     *
     * @param expectedErrorId
     * @return
     */
    public static Matcher<View> hasTextInputLayoutErrorText(final int expectedErrorId) {
        return new TypeSafeMatcher<View>() {
            @Override
            public boolean matchesSafely(View view) {
                if (!(view instanceof TextInputLayout)) {
                    return false;
                }
                CharSequence error = ((TextInputLayout) view).getError();
                if (error == null) {
                    return false;
                }
                String errorString = error.toString();
                String expectedError = view.getContext().getString(expectedErrorId);
                return expectedError.equals(errorString);
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("Error not present");
            }
        };
    }
}
