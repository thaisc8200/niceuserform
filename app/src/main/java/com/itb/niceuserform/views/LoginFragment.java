package com.itb.niceuserform.views;

import androidx.lifecycle.ViewModelProviders;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.itb.niceuserform.R;

public class LoginFragment extends Fragment {

    private LoginViewModel mViewModel;
    private MaterialButton btnLogin;
    private MaterialButton btnRegister;
    private TextInputLayout tilUsername;
    private TextInputLayout tilPassword;
    private Dialog dialog;

    public static LoginFragment newInstance() {
        return new LoginFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.login_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        btnLogin = view.findViewById(R.id.btnLLogin);
        btnRegister = view.findViewById(R.id.btnLRegister);
        tilUsername = view.findViewById(R.id.tilLUsername);
        tilPassword = view.findViewById(R.id.tilLPassword);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(LoginViewModel.class);
        // TODO: Use the ViewModel
        btnLogin.setOnClickListener(this::onLoginButtonClicked);
        btnRegister.setOnClickListener(this::onRegisterButtonClicked);
        mViewModel.getLoading().observe(getViewLifecycleOwner(), this::onLoadingChanged);
        mViewModel.getLogged().observe(getViewLifecycleOwner(), this::onLoggedChanged);
        mViewModel.getError().observe(getViewLifecycleOwner(), this::onErrorChanged);
    }

    private void onErrorChanged(String error) {
        Snackbar.make(getView(), error, Snackbar.LENGTH_LONG).show();
    }

    private void onLoggedChanged(Boolean logged) {
        if(logged){
            Navigation.findNavController(getView()).navigate(R.id.action_loginFragment_to_userLoggedFragment);
        }
    }

    private void onLoadingChanged(Boolean loading) {
        if (loading){
            dialog = ProgressDialog.show(getContext(), "Loading", "Loading...", true);
            dialog.show();
        } else {
            if(dialog != null) dialog.dismiss();
        }
    }

    private void onRegisterButtonClicked(View view) {
        Navigation.findNavController(view).navigate(R.id.action_loginFragment_to_registerFragment);
    }

    private void onLoginButtonClicked(View view) {
        if(validateLogin()) {
            mViewModel.login(tilUsername.getEditText().getText().toString().trim());
        }
    }

    private boolean validateLogin(){
        boolean isUsernameEmpty = tilUsername.getEditText().getText().toString().isEmpty();
        boolean isPasswordEmpty = tilPassword.getEditText().getText().toString().isEmpty();
        if (isUsernameEmpty) {
            tilUsername.setError(getString(R.string.required_field));
        }
        if (isPasswordEmpty) {
            tilPassword.setError(getString(R.string.required_field));
        }
        return !isUsernameEmpty && !isPasswordEmpty;
    }

}
