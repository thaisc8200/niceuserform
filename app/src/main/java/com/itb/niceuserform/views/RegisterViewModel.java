package com.itb.niceuserform.views;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.itb.niceuserform.retrofit.UserApi;
import com.itb.niceuserform.retrofit.UserApiProvider;
import com.itb.niceuserform.retrofit.UserSession;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterViewModel extends AndroidViewModel {
    private MutableLiveData<Boolean> logged = new MutableLiveData<>(false);
    private MutableLiveData<Boolean> loading = new MutableLiveData<>(false);
    private MutableLiveData<String> error = new MutableLiveData<>();
    private UserApi api;

    public RegisterViewModel(@NonNull Application application) {
        super(application);
        api = UserApiProvider.getUserApi();
    }

    public void register(String user){
        loading.setValue(true);
        Call<UserSession> call = api.register(user);
        call.enqueue(new Callback<UserSession>() {
            @Override
            public void onResponse(Call<UserSession> call, Response<UserSession> response) {
                loading.setValue(false);
                if(response.body().isLogged()){
                    logged.setValue(true);
                }else{
                    logged.setValue(false);
                    error.setValue(response.body().getError());
                }
            }

            @Override
            public void onFailure(Call<UserSession> call, Throwable t) {
                loading.setValue(false);
                error.setValue("Unknown error");
            }
        });
    }

    public LiveData<Boolean> getLogged(){
        return logged;
    }

    public LiveData<Boolean> getLoading(){
        return loading;
    }

    public LiveData<String> getError(){
        return error;
    }
}
